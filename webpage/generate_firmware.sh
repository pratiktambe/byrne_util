#!/bin/bash
boss_fw_version=
boss_serial=
while getopts v:s: option
do
        case "${option}"
        in
                v) boss_fw_version=${OPTARG};;
                s) boss_serial=${OPTARG};;
        esac
done
if [ -z "${boss_fw_version}" -o -z "${boss_serial}" ] ; then
   echo "BOSS fw version or BOSS serial not provided"
   echo "Usage: $0 -v 3.4.0 -s 20220"
   exit
fi
mkdir -p work
rm -rf work/*
if [ ! -f aparato-raw-${boss_fw_version}.tgz ]; then
    echo "BOSS Firmware input file not found!"
    exit 1
fi
if [ ! -f data -o ! -f upgrade-data -o ! -f upgrade.priv ]; then
    echo "BOSS input files not found!"
    exit 2
fi

tar xzf aparato-raw-${boss_fw_version}.tgz -C work/
openssl enc -aes-256-cbc -salt -in work/root.tar.gz -pass file:data -out work/root.tar.gz.enc
mv work/root.tar.gz.enc work/root.tar.gz
tar czf - -C work . | openssl enc -aes-256-cbc -salt -pass file:upgrade-data -out body
openssl dgst -sha1 -sign upgrade.priv -out sig < body
cat sig body > upgrade-${boss_fw_version}-${boss_serial}.bin
