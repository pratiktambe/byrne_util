#!/usr/bin/env python3

#Utility for Byrne Key Generation
#Author: Pratik M Tambe
#Date: 31st March 2017

#Requirements:
#JVM - JRE or JDK
#Oracle Client 12.2
#Python 2.7 +, Python-PIP
#jaydebeapi (Python + JDBC)

# Useful URLs to complete pre-requisites
# http://www.gilfillan.space/2016/04/24/install-oracle-instant-client-1604/
# https://pypi.python.org/pypi/JayDeBeApi

#imports I
import sys
import os
import time
import ConfigParser
import tarfile
import jaydebeapi

#imports II
from subprocess import Popen, PIPE
#import jpype
#One may comment the following line but I define this explicitly
os.environ['JAVA_HOME'] = '/usr/lib/jvm/java-8-openjdk-amd64/jre'
#jpype.startJVM(jpype.getDefaultJVMPath(), '-Djava.class.path=ojdbc8.jar')

# Byrne utility in Python
class Byrne:

    # Constructor to create a new byrne functions instance
    def __init__(self,boss_serial):
        self.conn = None
        self.curs = None
        # get the path of the place where the file is run from
        self.program_path = os.path.dirname(os.path.abspath(__file__))
        # read configuration file parameters for the panel
        configParser = None
        configParser = ConfigParser.RawConfigParser()
        configFilePath = str(self.program_path) + '/' + 'byrne_util.conf'
        configParser.read(configFilePath)
        self.db_username = str(configParser.get('DATABASE','USERNAME'))
        self.db_password = str(configParser.get('DATABASE','PASSWORD'))
        self.db_host = str(configParser.get('DATABASE','HOST'))
        self.db_port = str(configParser.get('DATABASE','PORT'))
        self.db_connection_string = 'jdbc:oracle:thin:'+self.db_username+'/'+self.db_password+'@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST='+self.db_host+')(PORT='+self.db_port+'))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=brivovac)))'
        self.db_connection_driver = 'oracle.jdbc.driver.OracleDriver'
        self.boss_serial = boss_serial
        self.compression_file_list=["upgrade.pub","upgrade.priv","license.pub","license.priv","backup.pub","backup.priv","upgrade-data","license-data","data"]
        # cleanup old data
        for i in range(len(self.compression_file_list)):
            os.remove(str(self.program_path) +"/../"+ self.compression_file_list[i]) if os.path.exists(str(self.program_path) +"/../"+ self.compression_file_list[i]) else None
        # remove all tgz tar files
        for root, dirs, files in os.walk(self.program_path + "/../"):
                for currentFile in files:
                    print "processing file: " + currentFile
                    exts = ('.tgz', '.tar')
                    if any(currentFile.lower().endswith(ext) for ext in exts):
                        os.remove(os.path.join(root, currentFile)) 

    # return boss_serial
    def get_boss_serial(self):
        return self.boss_serial

    def get_compression_file_list(self):
        return self.compression_file_list

    #  connect to the DB server 
    def connectToDB(self):
        try:
            self.conn = jaydebeapi.connect(self.db_connection_driver, self.db_connection_string)
        except:
            print("Sorry! Connection could not be established.")
            if self.curs != None:
                self.curs.close()
                self.curs = None
            if self.conn != None:
                self.conn.close()
                self.conn = None
        return self.conn
            #sys.exit()


    #  execute query at DB server 
    def executeSQL(self, sql):
        result=None
        try:
            self.curs = self.conn.cursor()
            self.curs.execute(sql)
            result = self.curs.fetchall()
        except:
            print("Sorry! Could not execute Query.")
            #sys.exit()
        return result


    #  disconnect from the DB server 
    def disconnectFromDB(self):
        if self.curs != None:
            self.curs.close()
            self.curs = None
        if self.conn != None:
            self.conn.close()
            self.conn = None
        #sys.exit()


    # write to the file
    def writeToFile(self,file_name,file_contents):
        with open(file_name,'w+') as f:
             f.write(str(file_contents))

    def createTarBall(self,tar_file_name,tar_file_compression_mode,files_to_add):
        tar = tarfile.open(tar_file_name, tar_file_compression_mode)
        for name in files_to_add:
            tar.add(name)
        tar.close()

"""
# Driver program to test above function
byrne = Byrne()


byrne.connectToDB()

for i in range(len(byrne.get_compression_file_list())):
    sql = "select encryption_keys.key from encryption_keys, appliances where encryption_keys.appliance_id = appliances.id and appliances.serial_number = "+byrne.get_boss_serial()+" and encryption_keys.encryption_key_type_id="+str(i+1)
    result=byrne.executeSQL(sql)
    print (result[0][0])
    byrne.writeToFile(byrne.get_compression_file_list()[i],result[0][0])


byrne.disconnectFromDB()

#Create tar ball (no compression) 
byrne.createTarBall(byrne.get_boss_serial()+"_keys.tar","w:",byrne.get_compression_file_list())
"""
