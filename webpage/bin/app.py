import web
from byrne_util import Byrne
import subprocess
import time
import os
import sys

urls = None
urls = (
  '/byrne', 'Index',
  '/upgrade','Upgrade',
)

app = None
app = web.application(urls, globals())

render = None
render = web.template.render('templates/', base="layout")

class Index(object):
    def GET(self):
        return render.byrne_keys()

    def POST(self):
        form = None 
        form = web.input(boss_serial="None")
        boss_serial = "%s" % (form.boss_serial)

        conn_ret = None
        # Driver program to test above function
        byrne = Byrne(boss_serial)
        conn_attempts = 0
        while conn_ret is None:
             conn_attempts += 1
             conn_ret = byrne.connectToDB()
             print("Connection return value "+ str(conn_ret))
             time.sleep(1)
             if conn_attempts >= 3:
                sys.exit()
        for i in range(len(byrne.get_compression_file_list())):
          try:
            sql = "select encryption_keys.key from encryption_keys, appliances where encryption_keys.appliance_id = appliances.id and appliances.serial_number = "+byrne.get_boss_serial()+" and encryption_keys.encryption_key_type_id="+str(i+1)
            result=byrne.executeSQL(sql)
            #print (result[0][0])
            byrne.writeToFile(byrne.get_compression_file_list()[i],result[0][0])
          except:
            print("Could not retrieve key from Database for "+ byrne.get_compression_file_list()[i])
            byrne.disconnectFromDB()
            return render.byrne_keys_not_found()
        byrne.disconnectFromDB()
        #Create tar ball (no compression) 
        byrne.createTarBall(byrne.get_boss_serial()+"_keys.tar","w:",byrne.get_compression_file_list())

        #Create Download
        filename = byrne.get_boss_serial()+'_keys.tar' #web.input().file
        path = byrne.get_boss_serial() + '_keys.tar' 
        web.header('Content-Disposition', 'attachment; filename=\"' + byrne.get_boss_serial()+'_keys.tar\"')
        web.header('Content-type','application/tar')
        web.header('Content-transfer-encoding','binary') 
        return open(path, 'rb').read()

        #return render.index(boss_serial = boss_serial)

class Upgrade(object):
    def GET(self):
        return render.byrne_upgrade_file()

    def POST(self):
        form = None 
        form = web.input(boss_serial="None", fw_version="None")
        boss_serial = "%s" % (form.boss_serial)
        print(boss_serial)
        fw_version = "%s" % (form.fw_version)
        print(fw_version)
        try:
            bin_path = os.path.dirname(os.path.abspath(__file__))
            command = bin_path+"/../generate_firmware.sh -v " + fw_version + " -s " + boss_serial
            print(command)
            proc = subprocess.Popen(command, shell=True)
            count = 0
            while proc.poll() is None:
                  count += 1
                  print("waiting to generate the file...")
                  time.sleep(1)
                  if count >= 10:
                     break;
        except:
            print("Generating upgrade bin file failed!")
            return render.byrne_upgrade_file_not_found()
        #check if the file is generated
        if not os.path.isfile('upgrade-'+fw_version+'-'+boss_serial+'.bin'):
            return render.byrne_upgrade_file_not_found()
        #Create Download
        filename = 'upgrade-'+fw_version+'-'+boss_serial+'.bin' #web.input().file
        path = 'upgrade-'+fw_version+'-'+boss_serial+'.bin'
        web.header('Content-Disposition', 'attachment; filename=\"upgrade-'+fw_version+'-'+ boss_serial+'.bin\"')
        web.header('Content-type','application/octet-stream')
        web.header('Content-transfer-encoding','binary') 
        return open(path, 'rb').read()

if __name__ == "__main__":
    app.run()
