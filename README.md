# README #

Recently Byrne has been giving us lot of problems.

This is a tool to ensure Byrne Keys tarball can be obtained in case Byrne application is down and Byrne DB is still up.

Byrne username and password have deliberately been masked while committing the code. This is to prevent unauthorized access.

Prerequisites:

JAVA JRE/JDK >= 8

oracle instant client >= 12.2
See

http://www.gilfillan.space/2016/04/24/install-oracle-instant-client-1604/

sudo apt-get install python3 python-pip python3-pip python-dev python3-dev

For python 2

pip install utils db lpthw.web JayDeBeApi

For python 3

pip3 install utils db lpthw.web JayDeBeApi 

Note:

For Python3 in the code keyword 'ConfigParser' is changed to keyword 'configparse' -> Code needs to be altered as of now

Checkout the code

Set the Byrne username and password in the config file

How to start the server?

cd webpage

vim bin/byrne_util.conf <------- set the Oracle byrne username and password

python bin/app.py

Open the browser (on local machine)

http://127.0.0.1:8080/byrne

Enter the BOSS serial number
Then Download the BOSS keys tarball file

**Note:** The RAW BOSS firmware file of the format aparato-raw-${boss_fw_version}.tgz should be present in the parent 'webpage' directory for the following to work. There can be multiple versions that co-exists simultaneously.

To generate BOSS firmware 

Open the browser (on local machine)

http://127.0.0.1:8080/upgrade

Enter the BOSS serial 

Enter the firmware version <--------- This should match one of the ${boss_fw_version} of tgz files. 

Click "Submit Query" button